#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int countArgs(char* buffer, char** args){
   int count = 0;
   char* str;
   stpcpy(str, buffer);
   //char str[80] = "This is - www.tutorialspoint.com - website";
   char s[2] = " ";
   char *token;
   
   /* get the first token */
   token = strtok(str, s);
   
   /* walk through other tokens */
   while(token != NULL){
      printf( " %s\n", token );
	  args[count] = token;
      count++;
      token = strtok(NULL, s);
   }

/**  
   int i = 0;
   for(i;i<=strlen(buffer);i++){    
   printf("%s\n", &buffer[i]);
     count ++;
   }   
   **/
   return count;
}

int main(int argc, char **argv){
  int a = 5;
  char** args = (char**)malloc(sizeof(char**)*10);

  int pid = fork(); 
  if (pid == 0) {

    //printf("child with pid of %d\n", getpid());

    char* argsTest = "ls -l";
    
    //argsTest[1] = "-l";
    //argsTest[2] = NULL;
    //int rc = execvp(argsTest[0], argsTest);
int res = countArgs(argsTest, args);
	printf("Count of Args %d\n", res);
	
	int i = 0;
	for(i; i<res;i++){
		printf("Args %s\n", args[i]);
	}
	
    //printf("got below exec\n");

  } else {

    //printf("parent with pid of %d just forked off child %d\n", getpid(), pid);
    //sleep(1);

    int status;
    int result = waitpid(pid, &status, 0);
    //int result = waitpid(pid, &status, WNOHANG);
    //int result = waitpid(-1, &status, 0);
    
  }

}
