#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/wait.h>

#define MAXLINE 512

int checkBuiltInCommands(char** args);
void shellPrompt();
void printArgs(char** args, int argCount);
void handleUserCommand(char** args, int* argCount);
int parseArgs(char* buffer, char** args);
void displayHistory();
void addToHistory(char** args, int argCount);
void launchJob(char** args, int* argCount);

typedef struct History{
	int argCount;
	char** args;
} History;

int histCount;	
History hist[25];

int main(int argc, char **argv){			
	char buffer[MAXLINE]; //buffer is to hold the commands that the user will type in
	shellPrompt(); //Display custom prompt	
	signal(SIGCHLD, SIG_IGN);  // SIG_IGN   SIG_DFL
	while(1){		
		fgets(buffer, MAXLINE, stdin); //get input			
		if(strcmp("\n", buffer) == 0) {
			shellPrompt();				
		}else{
			char** args = (char**)malloc(MAXLINE);		
			int argCount = 0;						
			argCount = parseArgs(buffer, args); 	
			args[argCount] = NULL;				
			
			if(args[0][0] == '!'){//Check if we are attempting to run something from the history
				int num = (int)(args[0][1] - 49);		
				if(num < histCount && num >= 0){			
					//args = hist[num].args;		
					//argCount = hist[num].argCount;		
					handleUserCommand(hist[num].args, &hist[num].argCount);	
				}
			}else{
				handleUserCommand(args, &argCount);			
			}
			
//printArgs(args, argCount);
			addToHistory(args, argCount);			
			
			//Clean up.
			int d;
			for(d = 0; d < argCount; d++){
				free(args[d]);
			}			
			free(args);
			
			shellPrompt();
		}
	}
	return 0;	
	}

int parseArgs(char* buffer, char** args){
   int count = 0;
   char str[MAXLINE];  
   char s[2] = " ";
   char* token;  
   stpcpy(str, buffer); //Copy the buffer over 
   token = strtok(str, s); // get the first token
   
   // walk through other tokens 
	while(token != NULL){ 	
		if('\n' == token[strlen(token) - 1]){//Strip out the new line if present			
			token[strlen(token) - 1] = '\0';
		}
		args[count] =  (char*) malloc(MAXLINE);
		strcpy(args[count], token);
		count++;	  							
		token = strtok(NULL, s);		
   }     
   return count;
}//END parseArgs

void shellPrompt(){
	//print the prompt
	char cwd[256];
	getcwd(cwd, sizeof(cwd));
	printf("%s>", cwd);
}//END shellPrompt

void printArgs(char** args, int argCount){
	int i = 0;
	for(i = 0; i < argCount;i++){	
		printf("%s", args[i]);
		printf(" ");		
	}	
	printf("\n");
}//END printArgs

void handleUserCommand(char** args, int* argCount){
	if (checkBuiltInCommands(args) == 0) {
		launchJob(args, argCount);
	}
}//END handleUserCommand

int checkBuiltInCommands(char** args){
	int result = 0;

	if (strcmp("exit", args[0]) == 0) {
		exit(EXIT_SUCCESS);
		result = 1;
	}
	if (strcmp("cd", args[0]) == 0) {
		if (chdir(args[1]) == -1) {
			printf(" %s: no such directory\n", args[1]);
		}
		result = 1;
	}
	if (strcmp("history", args[0]) == 0) {
		displayHistory();
		result = 1;
	}

	return result;
}//END checkBuiltInCommands

void displayHistory(){
	int i;
	for(i = 0; i < histCount; i++){
		char out[MAXLINE];
		int j;
		*out = '\0';			
		sprintf(out,"!%d", i + 1);
		for(j = 0; j < hist[i].argCount; j++){						
			strcat(out, " ");
			strcat(out, hist[i].args[j]);
			strcat(out, " ");
		}
		printf("%s\n", out);
	}
}//END displayHistory

void addToHistory(char** args, int argCount){		
	History new;		
	int i;
	//printf("mm %s\n", args[0]);	
	if(histCount < 25){
		new.args = (char**) malloc(MAXLINE);
		for(i = 0; i < argCount; i++){
			new.args[i] = (char*) malloc(sizeof(args[i]));
			new.args[i] = strcpy(new.args[i], args[i]);
		}		
		new.argCount = argCount;	
		hist[histCount] = new;	
		histCount++;	
	}
}//END addToHistory

void launchJob(char** args, int* argCount){
	char prog[MAXLINE];		
	char* path = "/bin/"; // /bin/program_name is the arguments to pass to execv
    bool isInRed = false;
	bool isOutRed = false;			
	char fileName[MAXLINE];
	int i = 0;
	
	for(i = 0; i < *argCount; i++){//check for redirection command
		if(strcmp("<", (args[i])) == 0){			
			isInRed = true;
		}else if(strcmp(">", (args[i])) == 0){
			isOutRed = true;
		}
	}
		
	if(isInRed || isOutRed){//Get the file name and remove it and the redirection command	
		strcpy(fileName, args[*argCount - 1]);//Get the name of the file					
		args[*argCount - 1] = NULL;		
		*argCount = *argCount - 1;
		args[*argCount - 1] = NULL;
		*argCount = *argCount - 1;
	}

	if(args[0][0] != '.' && args[0][1] != '/') {//Check if we are running a local program(starts with ./)
		strcpy(prog, path);//First we copy a /bin/ to prog
		strcat(prog, args[0]); //Then we concatenate the program name to /bin/
	}else{	
	   strcpy(prog, "");   
	   strcat(prog, args[0]);	   
	}

	int pid = fork();//Fork and monitor the action		
	if(pid ==0){//if pid == 0 then we're at the child  
		if(isInRed || isOutRed){//Redirect		
			if(isOutRed){
				freopen(fileName, "w", stdout);	
			}else{
				freopen(fileName, "r", stdin);
			}			
		}		
		int rv = execv(prog, args);
		printf("Error processing request.\n");
		exit(rv);		
	}else{//If pid !=0 then it's the parent
		int status;
		int result;				
		if(strcmp("&", (args[*argCount - 1])) == 0){//check if we want to run in the background		        
			result = waitpid(pid, &status, WNOHANG);			
		}else{			
			result = waitpid(pid, &status, 0);
		}	
	}
}//END launchJob