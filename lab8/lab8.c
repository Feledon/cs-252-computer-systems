#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <omp.h> // compile with -fopenmp
#include <unistd.h>  // usleep


double totalTime;

void delay() {
  usleep(1000*100); // 100 ms
}
bool findPrime(int n){//check if n is prime and return T/F
	int d = 2;	
	bool isPrime = true;
	while(d < n && isPrime){		
		if((n % d) == 0){
			isPrime = false;
		}else{
			d++;
			int i = omp_get_thread_num();
			//printf("#%d: %d\n", i, n);
		}		
	}

	return isPrime;	
}

int findPrimeBlocking(int beg, int end){
	int i;	
	double startTime;
	int primeCount = 0;	
	printf("\nBlocking:\n");		

	#pragma omp parallel reduction(+:primeCount) 
	{
		startTime = omp_get_wtime();		
		#pragma	omp for nowait
			  for (i = beg; i < end; i++){					
				if(findPrime(i)){					
					primeCount++;						
				}				
			  }								  
		printf("   Time for #%d: %f with %d found\n", omp_get_thread_num(), (omp_get_wtime() - startTime), primeCount);		
	}	  
	//endTime = omp_get_wtime();
	//totalTime = totalTime + (endTime - startTime);
	//printf("total: %d\n", totalTime);
	
	return primeCount;
}

int findPrimeStriping(int beg, int end){
	int i;	
	double startTime;	
	int totalTime = 0;
	int primeCount = 0;	
	printf("\nStriping:\n");		
	
	#pragma omp parallel reduction(+:primeCount)
	{
		startTime = omp_get_wtime();
	#pragma omp for 
		  for (i = beg; i < end; i++){					
				if(findPrime(i)){
					primeCount++;				
				}			
			}
		totalTime = totalTime + (omp_get_wtime() - startTime);			
	}
	printf("   Time: %f with %d found\n", totalTime, primeCount);	
	return primeCount;
}

void main(int argc, char** argv){	
	int beg = atoi(argv[1]);
	int end = atoi(argv[2]);	
	int threadNum = 5;	
	int i;
	int pr = 0;
	omp_set_num_threads(threadNum);
	
	//Do striping
	pr = findPrimeStriping(beg, end);

	//Do Blocking
	findPrimeBlocking(beg, end);

	printf("Overall %d threads found\n", pr);
}