#include <stdio.h>
#include <stdlib.h>
#include <omp.h> // compile with -fopenmp
#include <unistd.h>  // usleep


void delay() {
  usleep(1000*100); // 100 ms
}

int dotprod1(int* a, int* b, int* c, int n) {

  int sum = 0;
  
  int i;
#pragma omp parallel for
  for (i=0; i<n; i++) {

#pragma omp critical
    {
      int oldsum = sum;
      delay();
      sum = oldsum + (a[i] * b[i]);
    }

  }

  return sum;
}

int dotprod2(int* a, int* b, int* c, int n) {

  int nt;
#pragma omp parallel
  {
    nt = omp_get_num_threads();
    printf("%d\n", nt);
  }
  
  int sum[nt];
  int i;
  for (i=0; i<nt; i++){
    sum[i] = 0;
  }
  
#pragma omp for //nowait
  for (i=0; i<n; i++) {

    int oldsum = sum[omp_get_thread_num()];
    delay();
    sum[omp_get_thread_num()] = oldsum + (a[i] * b[i]);
  }

  int tsum = 0;
  for (i=0; i<4; i++){
    tsum += sum[i];
  }  

  return tsum;
}

int dotprod3(int* a, int* b, int* c, int n) {

  int sum = 0;
  
  int i;
#pragma omp parallel for reduction(+:sum)
  for (i=0; i<n; i++) {

    int oldsum = sum;
    delay();
    sum = oldsum + (a[i] * b[i]);

  }

  return sum;
}


void vecadd(int* a, int* b, int* c, int n) {

  int i;
#pragma omp parallel for schedule(static, 2) // static or dynamic
  for (i=0; i<n; i++) {
    printf("%d %d\n", omp_get_thread_num(), i);
    c[i] = a[i] + b[i];
  }

}


int main() {

  omp_set_num_threads(4);
  
  int n = 100;
  int a[n];
  int b[n];
  int c[n];

  int i;
  for(i=0; i<n; i++) {
    a[i] = i;
    b[i] = i;
  }

  // vecadd(a, b, c, n);
  int dp = dotprod3(a, b, c, n);

  printf("%d\n", dp);

  //  for(i=0; i<n; i++) {
  //    printf("%d\n", c[i]);
  //  }

  /*

  omp_set_num_threads(4);
  
  int i;
#pragma omp parallel
  {

#pragma omp for
  for(i=0; i<100; i++) {
    int tn = omp_get_thread_num();
    printf("%d: %d\n", i, tn);
  }

  }

#pragma omp parallel
  {
    printf("hello world\n");
    
    printf("bye\n");
  }

  printf("done\n");

  */
}