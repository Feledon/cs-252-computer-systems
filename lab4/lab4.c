#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct node {
  char* value;
  struct node* next;
} LinkedList;

LinkedList* llCreate();
void llDisplay(LinkedList* l);
void llAddAt(LinkedList** l, char* value, int index);
void llAdd(LinkedList** l, char* value);
void cleanUp(LinkedList* l);
int llSize(LinkedList* l);
void llRemoveAt(LinkedList** l, int index);
void llRemoveString(LinkedList** l, char* str);

int main() {
  LinkedList* l2 = llCreate();

  llAdd(&l2, "one");
  llAdd(&l2, "two");
  llAdd(&l2, "three");
  llAddAt(&l2, "kangaroo",0);
  llAddAt(&l2, "koala",2);

  llDisplay(l2);
  printf("Count: %d\n", llSize(l2));

  llRemoveString(&l2, "koala");
  llRemoveAt(&l2, 0);
  llRemoveAt(&l2, 100);
  llDisplay(l2);
  
  //Clean up and free 
  cleanUp(l2); 
}//END MAIN

void llRemoveString(LinkedList** l, char* str){
  LinkedList* p = *l;
  int index = 0;
  int cntr = 0;
  bool found = false;
  while(p != NULL){ 
    if(strcmp(p->value,str)==0){
       index = cntr; 
       found = true;   
    }	
    cntr++;
    p = p->next;
  }
  if(found){
     llRemoveAt(l, index);//reuse the index removal function
  }
}//END REMOVESTRING

void llRemoveAt(LinkedList** l, int index){
   int cntr = 0;
   LinkedList* victim = *l;
   LinkedList* parent;
   if(index <= llSize(*l)){//Not in the list      
	   if(index==0){
		  *l = victim->next;       
	   }else{
		  for(cntr; cntr < index; cntr++){
			 parent = victim;
			 victim = victim->next;   
		  }   
		  parent->next = victim->next;
	   }
	   free(victim->value);
	   free(victim);
   }
}//END REMOVEAT

void llAddAt(LinkedList** l, char* value, int index){
  LinkedList* newNode = (LinkedList*)malloc(1*sizeof(LinkedList));
  newNode->value = (char*)malloc(sizeof(char)*10);
  strcpy(newNode->value, value);
  if(index > llSize(*l)){//Bigger than out list, so add to the end
    llAdd(l, value);
  }else{
  if(index == 0){
    newNode->next = *l;
    *l = newNode;   
  }else{
    int i = 0;
    LinkedList* p = *l;
    for(i; i < (index - 1); i++){
      p = p->next;
    }
    newNode->next = p->next;
    p->next = newNode;
  }
  }
}//END ADDAT

LinkedList* llCreate(){
  return NULL;
}//END CREATE

void llDisplay(LinkedList* l){
  LinkedList* p = l;
  int ct = 0;
  printf("[");
  while(p != NULL){
    if(ct==0){
       printf("%s,", p->value);
    }else if(p->next != NULL){
       printf(" %s,", p->value);
    }else{
       printf(" %s", p->value);
    }
    ct++;
    p = p->next;
  }
  printf("]\n");
}//END DISPLAY

void llAdd(LinkedList** l, char* value){
  LinkedList* newNode = (LinkedList*)malloc(1*sizeof(LinkedList));
  newNode->value = (char*)malloc(sizeof(char)*10);
  strcpy(newNode->value, value);
  newNode->next = NULL;
  if (*l == NULL) {  // at to front/empty
    *l = newNode;
  } else {  // general case    
    LinkedList* p = *l;    
    while(p->next != NULL) {      
      p = p->next;
    }    
    p->next = newNode;
  }
}//END ADD

void cleanUp(LinkedList* l){
  LinkedList* p = l;
  LinkedList* p2;
  while(p != NULL){
    p2 = p->next;
    free((p)->value);
    free(p);
    p = p2;
  }
}//END CLEAN UP

int llSize(LinkedList* l){
  int count = 0;
  LinkedList* p = l;
  
  while(p != NULL) {
    p = p->next;
    count ++;
  }
  return count;
}//END SIZE
