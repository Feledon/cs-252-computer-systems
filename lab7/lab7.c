#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h> // -pthread compiler flag

int primeCount;
pthread_mutex_t lock;

bool findPrime(int n){//check if n is prime and return T/F	
	int d = 2;	
	bool isPrime = true;

	while(d < n && isPrime){		
		if((n % d) == 0){
			isPrime = false;
		}else{
			d++;
		}		
	}

	return isPrime;	
}//END findPrime

void* primeThread(void* parm){
	int* pa = (int*)parm;
	int beg = pa[0];
	int nthreads = pa[1];
	int threadNum = pa[2];
	int end = pa[3];	
	int localPrimeCount = 0;

	double div = (double)end /(double)nthreads;	
	int start = beg;
	if(threadNum != 0){
		start =	div * threadNum;
	}	
	int stop = ((double)end / (double)nthreads) * (threadNum + 1);
	localPrimeCount = findPrimeInRange(start, stop) + localPrimeCount;	
    //printf("thread #%d start = %d, stop = %d\n",threadNum, start, stop);	
	printf("Thread #%d found %d primes between %d and %d\n", threadNum, localPrimeCount, start, stop);
	
	pthread_mutex_lock(&lock);
	primeCount = primeCount + localPrimeCount;
	pthread_mutex_unlock(&lock);

	return NULL;	
}//END primeThread

int findPrimeInRange(int beg, int end){
	int localPrimeCount = 0;
	bool stop = false;
	
	while(beg < end){		
		if(findPrime(beg)){
			localPrimeCount++;
		}
		beg++;
	}		
	return localPrimeCount;
}//END findPrimeInRange

void getUserInput(int* beg, int* end, int* threadCount){
	printf("Enter a number to start our prime search at\n");
	scanf("%d", beg);
	
	printf("Enter a number to end our prime search at\n");
	scanf("%d", end);
	
	printf("Enter a number for how many threads to use\n");
	scanf("%d", threadCount);
}

void main(){	
	pthread_mutex_init(&lock, NULL);
	//int beg = 1000;
	//int end = 1000000;
	int beg = 10;
	int end = 100;	
	int threadCount = 4;
	primeCount = 0;
	
	getUserInput(&beg, &end, &threadCount);
		
	pthread_t ids[threadCount];
	int i;
	for (i=0; i < threadCount; i++) {
		int* p = (int*) malloc(4 * sizeof(int));
		p[0] = beg;
		p[1] = threadCount;
		p[2] = i;
		p[3] = end;

		pthread_create(&(ids[i]), NULL, primeThread, (void*) p);
	}			

	for (i = 0; i < threadCount; i++) {    
		pthread_join(ids[i], NULL);
	}			
	
	printf("There are %d primes between %d and %d\n", primeCount, beg, end);
	pthread_mutex_destroy(&lock);
}