#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

int primeCount;

bool findPrime(int n){//check if n is prime and return T/F
	//int n = 0;	
	int d = 2;	
	bool isPrime = true;

	//printf("Enter a number to if its prime.\n");
	//scanf("%d",&n);
	
	while(d < n && isPrime){		
		if((n % d) == 0){
			isPrime = false;
		}else{
			d++;
		}		
	}
	
	/**
	if(stop){
		printf("%d is not prime.\n", n);
	}else{
		printf("%d is prime.\n", n);
	}	
	**/
	return isPrime;	
}

int findPrimeInRange(int beg, int end){//Count the number of primes between beg and end and return the count
	int primeCount = 0;
	bool stop = false;
	
	while(beg < end){		
		if(findPrime(beg)){
			primeCount++;
		}
		beg++;
	}	
	
	return primeCount;
}

void getUserInput(int* beg, int* end){
	printf("Enter a number to start our prime search at\n");
	scanf("%d", beg);
	
	printf("Enter a number to end our prime search at\n");
	scanf("%d", end);
}

void main(){	
	//int beg = 1000;
	//int end = 1000000;;	

	int beg = 10;
	int end = 100;	
	
	getUserInput(&beg, &end);
	
	primeCount = findPrimeInRange(beg, end);
		
	printf("There are %d primes between %d and %d\n", primeCount, beg, end);

}