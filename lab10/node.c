#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>

/**
MUST ENTER PORT NUM WHEN RUN! ./node 7000
**/

int findPrimeInRange(int beg, int end);
bool findPrime(int n);

int findPrimeInRangeStriping(int beg, int end, int nodeNum, int numNodes){
	int localPrimeCount = 0;
	printf("%d -> %d\n", beg, end);
	while(beg < end){	
	//printf("%d -> %d\n", beg, end);	
		if(findPrime(beg + nodeNum)){
			localPrimeCount++;
		}
		beg += numNodes;
	}	
	
	//System.out.println("Thread #" + nodeNum + " multiples of " + beg + " + " + nodeNum + " found " + localPrimeCount);
	printf("Thread # %d found %d between %d and %d nodeNum %d\n", nodeNum, localPrimeCount, beg, end, numNodes);
	return localPrimeCount;
}

int main(int argc, char** argv){
	int beg = 0;
	int end = 0;	
	int primeCount = 0;
	
	int port = atoi(argv[1]);
	
	
  // Get a socket of the right type
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    printf("ERROR opening socket");
    exit(1);
  }

  // port number
  int portno = port;

  // server address structure
  struct sockaddr_in serv_addr;

  // Set all the values in the server address to 0
  memset(&serv_addr, '0', sizeof(serv_addr)); 

  // Setup the type of socket (internet vs filesystem)
  serv_addr.sin_family = AF_INET;

  // Basically the machine we are on...
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  // Setup the port number
  // htons - is host to network byte order
  // network byte order is most sig bype first
  //   which might be host or might not be
  serv_addr.sin_port = htons(portno);

  // Bind the socket to the given port
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    printf("ERROR on binding\n");
    exit(1);
  }

  // set it up to listen
  listen(sockfd,5);
  

  int newsockfd;
  struct sockaddr_in cli_addr;
  socklen_t clilen = sizeof(cli_addr);

  // Wait for a call
  newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  if (newsockfd < 0) {
    printf("ERROR on accept");
    exit(1);
  }




  
    /**
  Actual Work
  **/
  
	char buffer[256];
	memset(&buffer, '\0', 256);
	//  bzero(buffer,256);
	int inputs[4];
	//memset(&inputs, int, sizeof(inputs) * 4);
	int n = read(newsockfd,inputs,sizeof(inputs) * 4);
	if (n < 0) {
	printf("ERROR reading from socket\n");
		exit(1);
	}
	beg = inputs[0];
	end = inputs[1];
	int numNodes = inputs[2];
	int nodeNum = inputs[3];
printf("1: %d 2: %d 3: %d 4: %d\n", beg, end, numNodes, nodeNum);
	primeCount = findPrimeInRangeStriping(beg, end, nodeNum, numNodes);		
	//primeCount = findPrimeInRange(beg, end);		
	//printf("Found %d primes between %d and %d on %d\n", primeCount, beg, end, port);  	
	n = write(newsockfd, &primeCount ,sizeof(int));
	
  if (n < 0) {
    printf("ERROR writing to socket\n");
    exit(1);
  }
  
  /**
  Actual Work
  **/
  
  
  close(newsockfd);
  close(sockfd);

	return primeCount;
}

int findPrimeInRange(int beg, int end){
	int localPrimeCount = 0;
	bool stop = false;
	
	while(beg < end){		
		if(findPrime(beg)){
			localPrimeCount++;
		}
		beg++;
	}		
	return localPrimeCount;
}//END findPrimeInRange

bool findPrime(int n){//check if n is prime and return T/F	
	int d = 2;	
	bool isPrime = true;

	while(d < n && isPrime){		
		if((n % d) == 0){
			isPrime = false;
		}else{
			d++;
		}		
	}
	return isPrime;	
}//END findPrime