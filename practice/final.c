#include <stdio.h>

void getRed(){
   unsigned long color = 60194632;//00000011 10010110 01111111 01001000
   unsigned long mask = 16711680;//00000000 11111111 00000000 00000000
								 //00000000 10010110 00000000 00000000 

   color = color & mask;
   
   printf("Total: %d\n", color);
}

void pointerWork(){
//	getRed();
/** makes pointer from integer without a cast
int* a;
a = 5;
**/

/** Compiles, but seg faults when run
int* a;
*a = 5;
**/

/** Fails to complie, attempts to assign pointer value of 5
int a;
*a = 5;
**/

/**fails to compile, does not recognize left value as assignable variable
int a;
&a = 5;

printf("Number %d\n",a);
**/
}
typedef struct foo {
	int a; 
	int* b; 
} Foo;

int main() { 
	Foo f0; 
	f0.a = 5; 
	int x[10]; 
	int i = 0;
	f0.b = x; 
	Foo f1; 
	f1.a = 5; 
	f1.b = (int*)malloc(10*sizeof(int));
	
	Foo* f2 = (Foo*)malloc(10*sizeof(Foo)); 
	
	for (i=0; i<10; i++) { 
		f2[i].a = i; 
		f2[i].b = (int*)malloc(10*sizeof(int)); 
	}
	for (i=0; i<10; i++) { 
		free(f2[i].b);
	}
	free(f2);
	free(f1.b);
}

