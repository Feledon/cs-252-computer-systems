#include <stdio.h>
#include <stdbool.h>

int MAX_FILE_ENTRIES = 100;

typedef struct freq {
   int number;
   int count;
} freq;

void readScores(int* ar, int* size);
void displayScores(int* ar, int* size);
void calcHistogram(int* ar, int* size, freq* hist, int* histSize);
int sumHistogram(int* ar, int* size);
void displayHistogram(freq* ar, int* size);

main(){
   int size = 0;
   int ar[MAX_FILE_ENTRIES];

   readScores(ar, &size);

   displayScores(ar, &size);

   freq hist[size];
   int histSize = 0;

   calcHistogram(ar, &size, hist, &histSize);
   displayHistogram(hist, &histSize);
   int summed = sumHistogram(ar, &size);
   printf("Total of all inputs: %d\n", summed);
}


int sumHistogram(int* ar, int* size){
   int i = 0;
   int total = 0;
   for(i; i < *size; i++){
      total = total + (*(ar + i));
   }
return total;
}

void displayHistogram(freq* ar, int* size){
   int i = 0;
   for(i; i < *size; i++){
	freq curr = (*(ar + i));
	printf("Value: %d Freq: %d\n", curr.number, curr.count);     
   }
}

void calcHistogram(int* ar, int* size, freq* hist, int* histSize){
   int i = 0;
   int j = 0;
   for(i; i <= *size; i++){
	bool found = false;
        int current = *(ar + i);
	for(j = 0; j <= *histSize; j++){
	   int currentHist = (*(hist + j)).number;
	   if(current == currentHist){
		(*(hist + j)).count+=1;
		found = true;
	   }	    
	}
	if(!found){
	   *histSize+=1;
	   (*(hist + *histSize)).number = current;
	   (*(hist + *histSize)).count = 1;
	}
   }
}

void displayScores(int* ar, int* size){
   int i =  0;
   for(i; i < *size; i++){
	printf("score %d: %d\n", i, *(ar + i));
   }
}

void readScores(int* ar, int* size){
   int i = 0;
   
   for(i; i < MAX_FILE_ENTRIES; i++){  

	int in;
	if(scanf("%d",&in) != 1){
	   break;
	}  	
        *(ar + i) = in;
   }

   *size = i;
}

