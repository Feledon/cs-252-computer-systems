#ifndef set_h
#define set_h

#include "block.h"
#include <iostream>
#include <vector>
#include <math.h> 
#include <bitset>

using namespace std;

class Set{
private:
  vector<Block> list_of_blocks;
  int num_blocks;  
  int block_size;   
  unsigned long block_offset;
  
public:
	unsigned long set_index;
	Set(int block_size, int num_blocks);
	int Read(unsigned long memory_addr);
	int Write(unsigned long memory_addr);
	void Display();	
};
#endif