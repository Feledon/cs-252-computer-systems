#include "block.h"

Block::Block(int block_size) {
  //cout << "default const for block" << endl;
  this->block_size = block_size;  
  this->isV = 0;
  this->isD = false;
  this->tag_value = 0;
  
}

int Block::isValid(){
	return this->isV;
}

bool Block::isDirty(){
	return this->isD;
}
