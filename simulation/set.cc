#include "set.h"

Set::Set(int block_size, int num_blocks){
	//cout << "default const for set" << endl;
  this->num_blocks = num_blocks;  
  this->block_size = block_size; 
  this->block_offset = log2(block_size);
  
  int i = 0;
  for(i = 0; i < num_blocks; i++){
 	Block temp(block_size);
	unsigned long bit = i;
	bit = (bit << (block_offset));	
	//bitset<32> bs2 =  set_index;
	
	cout << "set index 2:" << bitset<32>(set_index) << " " << set_index << endl;
	//cout << "bit bs2 " << bitset<32>(bit) << " " << bs2 << endl;
	temp.tag_value = (bitset<32>(bit) | bitset<32>(set_index)).to_ulong(); 	
	cout << "set index 5:" << bitset<32>(temp.tag_value) << " " <<  temp.tag_value << endl;
	list_of_blocks.push_back(temp);
  }  
}
int Set::Read(unsigned long memory_addr){
	return 0;
}
int Set::Write(unsigned long memory_addr){
	return 0;
}
void Set::Display(){
	unsigned int i = 0;
	
	for(i = 0; i < list_of_blocks.size(); i++){		
		cout << "Block " << i << " isDirty() " << list_of_blocks[i].isDirty() << endl;		
	}	
}