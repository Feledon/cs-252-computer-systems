#ifndef block_h
#define block_h

#include <iostream>

using namespace std;

class Block {
private:
  int block_size;
  int isV;
  bool isD;
  
public:
	unsigned long tag_value;  
	Block(int block_size);
	int isValid();
	bool isDirty();
};
#endif