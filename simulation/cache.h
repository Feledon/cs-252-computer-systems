#ifndef cache_h
#define cache_h

#include "set.h"
#include <iostream>
#include <vector>
#include <math.h> 
#include <bitset>

using namespace std;

class Cache {
private:
  vector<Set> list_of_sets;
  int main_mem_space;
  int size_of_cache; 
  int block_size; 
  int set_assoc;
  int S;
  
public:
	int set_index_size;
	Cache(int main_mem_space, int size_of_cache, int block_size, int set_assoc);
	int Read(unsigned long memory_addr);
	int Write(unsigned long memory_addr);
	void Display();
	int DumpCache();
};
#endif