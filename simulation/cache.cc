#include "cache.h"


/**
It should contain a constructor that takes in the size of the 
main memory address space (m in the book), 
the size of the cache in bytes (C in the book), 
the size of the blocks (B in the book), 
and the set associativity (E in the book).  
	Set associativity is the number of cache lines/blocks per set.  
Everything else you need should be able to be determined from those values. 
Cache cache = new Cache(m, C, B, E);
**/
Cache::Cache(int main_mem_space, int size_of_cache, int block_size, int set_assoc){
  //cout << "default const for cache" << endl;
  this->main_mem_space = main_mem_space;  //m = log_2(S)
  this->size_of_cache = size_of_cache; //C = B * E * S
  this->block_size = block_size;//B = 2^b
  this->set_assoc = set_assoc;//E = #blocks/set 
  this->S = size_of_cache/(block_size * set_assoc);//S = C/(B*E)
  this->set_index_size = log2(S);//s #set index bits
  //M = 2^m max unique memory
  //b = log2(B) #block offset bits
  //t = m - (s + b) #tag bits
  
  
  //Cache* c1 = new Cache(m, C, B, E);
  //Cache* c1 = new Cache(4, 8, 2, 1);  
  //loop through for S to create that many sets  
  int i = 0;
  for(i = 0; i < S; i++){
	unsigned long bit = i;	
	
	bit = (bit << (set_index_size));//Shift the set index into the correct space
	cout << "set_index 1:" << bitset<32>(bit) << " " << bit << endl;
	
	Set temp(block_size, set_assoc);	
	temp.set_index = bit;
	list_of_sets.push_back(temp);
  }  
}

/**
The Cache class should also have methods to simulate a 
read and a write (those are good names for them!), 
which should take in a memory address (unsigned long works well for that).  
These methods should return a single int which will indicate how long 
the operation took: 0 for a cache hit, 1 for a cache miss, and 2 for a cache miss 
with a write-back on eviction. 
**/
int Cache::Read(unsigned long memory_addr){
	int hit = 1;	
	unsigned int i = 0;
	for(i = 0; i < list_of_sets.size(); i++){
		if(list_of_sets[i].set_index == ((memory_addr >> 0) & ((1 << 6) -1))){//(address >> 0) & ((1 << 6) -1)
			hit = list_of_sets[i].Read(memory_addr);
			//cout << "Found " << list_of_sets[i].set_index  << endl;
		}
	}	
	return hit;
}
int Cache::Write(unsigned long memory_addr){
	return 1;
}

/**
The Cache class should have a display method to 
dump the contents of the cache out to the screen for debugging.    
**/
void Cache::Display(){
	unsigned int i = 0;
	for(i = 0; i < list_of_sets.size(); i++){
		list_of_sets[i].Display();
	}
	//cout << "Displayed" << endl;
}

/**
There should also be a method to dump the cache which the program can call at the end.  
This should simply return how many cache values needed to be written back to main memory. 
**/
int Cache::DumpCache(){
	return 0;
}