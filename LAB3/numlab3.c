#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct Histogram {
   int num;
   int count;
} Histogram;

int* readScores(int* size);
void displayScores(int* ar, int size);
int* calcHistogram(Histogram** structAr, int* ar, int size);
void displayHistograms(Histogram* ar, int* size);
void sortHistograms(Histogram* ar, int* size);

int main(){
	int size = 0;
	int* ar = readScores(&size);
	displayScores(ar, size);
	Histogram* hist;   
	printf("%d\n", size);     
	int* histSize = calcHistogram(&hist, ar, size);
	displayHistograms(hist, histSize);

	sortHistograms(hist, histSize);
	printf("\n");
	displayHistograms(hist, histSize);	
	
	//Clean up
	free(histSize);
	free(hist);
	//free(size);
	free(ar);
}//END MAIN

int* calcHistogram(Histogram** hist, int* ar, int size){
	int i = 0;
	int j = 0;
	int* tempSize = (int*) malloc(sizeof(int));
	*tempSize = 0;	

	Histogram* structAr = (Histogram*) malloc(size * sizeof(Histogram*));

	for(i; i < size; i++){
	   int num = ar[i];
	   bool found = false;
	   for(j = 0;j < *tempSize; j++){
		 int thisNum = structAr[j].num;   		 
		 if(num == thisNum){		
			structAr[j].count += 1;
			found = true;   
		 }
	   }
	   if(!found){  
		  structAr[*tempSize].num = num;
		  structAr[*tempSize].count = 1;
		  *tempSize += 1;
	   }
	}
	*hist = structAr;
	return tempSize;
}//END calcHistogram

int* readScores(int* size){
	int test;
	int i = 0;
	int* temp = (int*)malloc(sizeof(int*));
	int* ar = (int*) malloc(100 * sizeof(int));

	for(i; i <= 100; i++){
		ar[i] = 0;
	}
	for(i=0; i < 100; i++){
	  if(scanf("%d", &test) != 1){      
		 break;
	  }
	 ar[i] = test;
	}
	*temp = i;
	*size = *temp;
	//free(temp);
	return ar;
}//END readScores

void sortHistograms(Histogram* ar, int* size){
	int i = 0;
	int j = 0;	
	int minIndex = 0;
	for(i; i< *size; i++){
	   minIndex = i;
	   for(j = i; j < *size; j++){
		  if(ar[minIndex].count < ar[j].count){
			 minIndex = j;
		  }
	   }
	   Histogram temp = ar[i];
	   ar[i] = ar[minIndex];
	   ar[minIndex] = temp;
	}
}//END sortHistograms

void displayHistograms(Histogram* ar, int* size){
	int i = 0;
	for(i; i < *size;i++){
	  Histogram curr = ar[i];    
	  printf("value %d freq %d\n", curr.num, curr.count);
	}
}//END displayHistograms

void displayScores(int* ar, int size){
	int i = 0;
	for(i;i<size;i++){
	  printf("%d\n", ar[i]);
	}
}//END displayScores
