#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct Histogram {
	int count;
	char* value;
} Histogram;

char** readScores(int* size);
void displayScores(char** ar, int size);
int calcHistogram(Histogram** structAr, char** ar, int size);
void displayHistograms(Histogram* ar, int size);
void sortHistograms(Histogram* ar, int size);

int main(){
	int size = 0;
	char** ar = readScores(&size);
	displayScores(ar, size);
	Histogram* hist;   
	printf("\n");     
	int histSize = calcHistogram(&hist, ar, size);
	displayHistograms(hist, histSize);

	sortHistograms(hist, histSize);
	printf("\n");
	displayHistograms(hist, histSize);	
	
	//Clean up
	int i = size;
	for(i; i >= 0; i--){
	   free(ar[i]);	
	}
	free(ar);
	free(hist);
}//END MAIN

int calcHistogram(Histogram** hist, char** ar, int size){
	int i = 0;
	int j = 0;
	int tempSize = 0;	

	Histogram* structAr = (Histogram*) malloc(size * sizeof(Histogram));

	for(i; i < size; i++){
	   bool found = false;
	   for(j = 0;j < tempSize; j++){
		 if(strcmp(structAr[j].value, ar[i])==0){		
			structAr[j].count += 1;
			found = true;   
		 }
	   }
	   if(!found){  
		  structAr[tempSize].value = ar[i];
		  structAr[tempSize].count = 1;
		  tempSize += 1;
	   }
	}
	*hist = structAr;
	return tempSize;
}//END calcHistogram

char** readScores(int* size){
	char** fromFile = (char**)malloc(100 * sizeof(char*));
	char* temp = (char*)malloc(100 * sizeof(char));

        while (scanf("%s", temp) != EOF){
                fromFile[*size] = temp;
                temp = (char*)malloc(100 * sizeof(char));
                (*size)++;
        }
        free(temp);
        return fromFile;
}//END readScores

void sortHistograms(Histogram* ar, int size){
	int i = 0;
	int j = 0;	
	int minIndex = 0;
	for(i; i< size; i++){
	   minIndex = i;
	   for(j = i; j < size; j++){
		  if(ar[minIndex].count < ar[j].count){
			 minIndex = j;
		  }
	   }
	   Histogram temp = ar[i];
	   ar[i] = ar[minIndex];
	   ar[minIndex] = temp;
	}
}//END sortHistograms

void displayHistograms(Histogram* ar, int size){
	int i = 0;
	for(i; i < size;i++){
	  Histogram curr = ar[i];    
	  printf("value %s freq %d\n", curr.value, curr.count);
	}
}//END displayHistograms

void displayScores(char** ar, int size){
	int i = 0;
	for(i;i<size;i++){
	  printf("%s\n", ar[i]);
	}
}//END displayScores
