#include <iostream>
#include <vector>
#include <climits>
#include "Set.h"
#include "Block.h"

using namespace std;

Set::Set(int numBlocks, int blockSize, int setNum) {
	this->numBlocks = numBlocks;
	this->blockSize = blockSize;
	this->blocks = createSet(setNum);
}

vector <Block> Set::createSet(int setNum){
	vector <Block> theSet;
	int i;
	for(i = 0; i < numBlocks; i++){
		Block block(blockSize, setNum);
		theSet.push_back(block);
	}
	return theSet;
}

int Set::read(int tag, int setIndex, int blockOffset) {
	
	int i;
	int miss = 0;
	Block* currBlock = new Block(blockSize, setIndex);
	
	//Check if we have a hit, if so, we are done
	for(i = 0; i < numBlocks; i++){
		miss = 0;
		int currTag = blocks[i].getTag();
		int currValid = blocks[i].getValid();
		if(currTag == tag && currValid != 0){	
			return 0;// hit
		} else {
			miss = 1;
		}
	}

	// miss
	if(miss == 1){
		// go through list and as soon as an empty is found, return 1		
		for(i = 0; i < numBlocks; i++){
			if(blocks[i].getValid() == 0){
				currBlock = &blocks[i];
				currBlock->setValid();
				currBlock->setTag(tag);
				currBlock->setSetIndex(setIndex);
				currBlock->setBlockOffset(blockOffset);
				currBlock->updateTime();
				if(currBlock->getDirty() == 1){
					return 2;
				} else {
					return 1;
				}
			}
		}
		// if no empty was found and returned, go through the list again to find the LRU and return 2
		//Set to the maxs so that on the first lap through we can look for < than	
		int prevHour = INT_MAX;
		int prevMin = INT_MAX;
		int prevSec = INT_MAX;
		int prevUsec = INT_MAX;	
		for(i = 0; i < numBlocks; i++){
			int currHour = blocks[i].getHour();
			int currMin = blocks[i].getMin();
			int currSec = blocks[i].getSec();
			int currUsec = blocks[i].getUsec();
			if(currHour == prevHour && currMin == prevMin && currSec == prevSec && currUsec < prevUsec){
//cout << "No empties currHour:" << currHour << " currMin:" << currMin << " currSec:" << currSec << " currUsec:" << currUsec << endl;		
//cout << "No empties prevHour:" << prevHour << " prevMin:" << prevMin << " prevSec:" << prevSec << " prevUsec:" << prevUsec << endl;	
				currBlock = &blocks[i];
			} else if (currHour == prevHour && currMin == prevMin && currSec < prevSec){
				currBlock = &blocks[i];
			} else if (currHour == prevHour && currMin < prevMin){
				currBlock = &blocks[i];
			} else if (currHour < prevHour){
				currBlock = &blocks[i];
			}
				prevHour = currHour;
				prevMin = currMin;
				prevSec = currSec;
				prevUsec = currUsec;
		}
		
		currBlock->setValid();
		currBlock->setTag(tag);
		currBlock->setSetIndex(setIndex);
		currBlock->setBlockOffset(blockOffset);
		currBlock->updateTime();
		if(currBlock->getDirty() == 1){
			currBlock->clearDirty();
//cout << "Here" << endl;			
			return 2;
		} else {
		
			return 1;
		}
	}

	return 0;
}

int Set::write(int tag, int setIndex, int blockOffset) {
	
	int i;
	int miss = 0;
	Block* currBlock = new Block(blockSize, setIndex);
	for(i = 0; i < numBlocks; i++){
		miss = 0;
		int currTag = blocks[i].getTag();
		int currValid = blocks[i].getValid();
		int currBlockOffset = blocks[i].getBlockOffset();
		if(currTag == tag && currValid != 0 && currBlockOffset == blockOffset){
			// hit
			currBlock = &blocks[i];
			currBlock->setDirty();
			return 0;
		} else {
			miss = 1;
		}
	}

	// miss
	if(miss == 1){
		// go through list and as soon as an empty is found, return 1
		for(i = 0; i < numBlocks; i++){
			if(blocks[i].getValid() == 0){
				currBlock = &blocks[i];
				currBlock->setValid();
				currBlock->setTag(tag);
				currBlock->setSetIndex(setIndex);
				currBlock->setBlockOffset(blockOffset);
				currBlock->updateTime();
				currBlock->setDirty();
				if(currBlock->getDirty() == 1){
					return 2;
				} else {
					return 1;
				}
			}
		}
		// if no empty was found and returned, go through the list again to find the LRU and return 2	
		int prevHour = INT_MAX;
		int prevMin = INT_MAX;
		int prevSec = INT_MAX;
		int prevUsec = INT_MAX;	
		for(i = 0; i < numBlocks; i++){
			int currHour = blocks[i].getHour();
			int currMin = blocks[i].getMin();
			int currSec = blocks[i].getSec();
			int currUsec = blocks[i].getUsec();
			if(currHour == prevHour && currMin == prevMin && currSec == prevSec && currUsec < prevUsec){
				currBlock = &blocks[i];
			} else if (currHour == prevHour && currMin == prevMin && currSec < prevSec){
				currBlock = &blocks[i];
			} else if (currHour == prevHour && currMin < prevMin){
				currBlock = &blocks[i];
			} else if (currHour < prevHour){
				currBlock = &blocks[i];
			}
				prevHour = currHour;
				prevMin = currMin;
				prevSec = currSec;
				prevUsec = currUsec;
		}
		
		currBlock->setValid();
		currBlock->setTag(tag);
		currBlock->setSetIndex(setIndex);
		currBlock->setBlockOffset(blockOffset);
		currBlock->updateTime();
		currBlock->setDirty();
		if(currBlock->getDirty() == 1){//Have we written to this block?
			currBlock->clearDirty();
			return 2;//Return the write-back information
		} else {
			return 1;//Return the miss
		}
	}

	return 0;
}

void Set::display() {
	int i = 0;
	for(i = 0; i < numBlocks; i++){
		cout << "Block Number: " << i << endl;
		cout << "Valid: " << blocks[i].getValid() << " Tag: " << blocks[i].getTag() << " Set Index: " << blocks[i].getSetIndex() << " Block Offset: " <<  blocks[i].getBlockOffset() << endl;
	}
}
int Set::countDirty() {
	int i = 0;
	int numDirty = 0;
	for(i = 0; i < numBlocks; i++){
		numDirty += blocks[i].getDirty();
	}
	return numDirty;
}