#include <iostream>
#include <vector>
#include <cmath>
#include <climits>
#include "Cache.h"
#include "Set.h"
#include "Block.h"

using namespace std;

/**
It should contain a constructor that takes in the size of the 
main memory address space (m in the book), 
the size of the cache in bytes (C in the book), 
the size of the blocks (B in the book), 
and the set associativity (E in the book).  
	Set associativity is the number of cache lines/blocks per set.  
Everything else you need should be able to be determined from those values. 
Cache cache = new Cache(m, C, B, E);
**/
Cache::Cache(int m, int C, int B, int E) {
  this->m = m; // address size (bits?)
  this->C = C; // cache size(bytes)
  this->B = B; // block size(bytes)
  this->E = E; // set accociativity (blocks per set)
  int setAmount = (C / B) / E;
  this->numSets = setAmount;
  this->sets = createCache();
  numDirty = 0;
}

vector <Set> Cache::createCache(){
	vector <Set> cache;
	int i;
	for(i = 0; i < numSets; i++){
		Set theSet(E, B, i);
		cache.push_back(theSet);
	}
	return cache;
}

void Cache::display() {
	int i = 0;
	for(i = 0; i < numSets; i++){
		cout << "----------------" << endl;
		cout << "Set Number: " << i << endl;
		cout << "----------------" << endl;
		sets[i].display();
	}
}
int Cache::countDirty() {
	int i = 0;
	int numDirty = 0;
	for(i = 0; i < numSets; i++){
		numDirty += sets[i].countDirty();
	}
	return numDirty;
}

/**
The Cache class should also have methods to simulate a 
read and a write (those are good names for them!), 
which should take in a memory address (unsigned long works well for that).  
These methods should return a single int which will indicate how long 
the operation took: 0 for a cache hit, 1 for a cache miss, and 2 for a cache miss 
with a write-back on eviction. 
**/
int Cache::read(unsigned long address) {
	// look to see if in cache
	// if cache hit, return 0
	// if cache miss, return 1
	// if cache miss with write-back, return 2
	
	int setIndexNumBits = log2(numSets);
	int blockOffsetNumBits = log2(B);
	int tagNumBits = m - setIndexNumBits - blockOffsetNumBits;
	
	/*---------------setting tag----------------*/	
	int k = 0;
	int tagStart = m - 1;
	int tagEnd = tagStart - tagNumBits;	
	int tag = 0;
	int tagSpot = tagNumBits - 1;
	for(k = tagStart; k > tagEnd; k--){ 
		tag = tag | ((address >> k & 1) << tagSpot);
		tagSpot--;
	}
	
	/*---------------setting setIndex----------------*/	
	k = 0;
	int setIndexStart = m - 1 - tagNumBits;
	int setIndexEnd = setIndexStart - setIndexNumBits;	
	int setIndex = 0;
	int setIndexSpot = setIndexNumBits - 1;
	for(k = setIndexStart; k > setIndexEnd; k--){ 
		setIndex = setIndex | ((address >> k & 1) << setIndexSpot);
		setIndexSpot--;
	}
	
	/*-------------setting blockOffset------------------*/	
	k = 0;
	int blockOffsetStart = m - 1 - tagNumBits - setIndexNumBits;
	int blockOffsetEnd = blockOffsetStart - blockOffsetNumBits;	
	int blockOffset = 0;
	int blockOffsetSpot = blockOffsetNumBits - 1;
	for(k = blockOffsetStart; k > blockOffsetEnd; k--){ 
		blockOffset = blockOffset | ((address >> k & 1) << blockOffsetSpot);
		blockOffsetSpot--;
	}
	/*-------------------------------*/
	
	return sets[setIndex].read(tag, setIndex, blockOffset);
	
}

int Cache::write(unsigned long address) {
	/** look to see if in cache
	if cache hit, return 0
	if cache miss, return 1
	if cache miss with write-back, return 2
	**/
	
	int setIndexNumBits = log2(numSets);
	int blockOffsetNumBits = log2(B);
	int tagNumBits = m - setIndexNumBits - blockOffsetNumBits;	
	
	/*----------------setting tag---------------*/	
	int k = 0;
	int tagStart = m - 1;
	int tagEnd = tagStart - tagNumBits;	
	int tag = 0;
	int tagSpot = tagNumBits - 1;
	for(k = tagStart; k > tagEnd; k--){ 
		tag = tag | ((address >> k & 1) << tagSpot);
		tagSpot--;
	}
	
	/*--------------setting setIndex-----------------*/	
	k = 0;
	int setIndexStart = m - 1 - tagNumBits;
	int setIndexEnd = setIndexStart - setIndexNumBits;	
	int setIndex = 0;
	int setIndexSpot = setIndexNumBits - 1;
	for(k = setIndexStart; k > setIndexEnd; k--){ 
		setIndex = setIndex | ((address >> k & 1) << setIndexSpot);
		setIndexSpot--;
	}
	
	/*--------------setting blockOffset-----------------*/	
	k = 0;
	int blockOffsetStart = m - 1 - tagNumBits - setIndexNumBits;
	int blockOffsetEnd = blockOffsetStart - blockOffsetNumBits;	
	int blockOffset = 0;
	int blockOffsetSpot = blockOffsetNumBits - 1;
	for(k = blockOffsetStart; k > blockOffsetEnd; k--){ 
		blockOffset = blockOffset | ((address >> k & 1) << blockOffsetSpot);
		blockOffsetSpot--;
	}
	/*-------------------------------*/
	
	return sets[setIndex].write(tag, setIndex, blockOffset);
}

int Cache::getNumDirty(){
	return numDirty;
}

int Cache::getNumSets(){
	return numSets;
}