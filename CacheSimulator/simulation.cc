#include <iostream>
#include <bitset>
#include "Cache.h"

using namespace std;
typedef unsigned long bitSet;

int main(){
	Cache cache(4, 8, 2, 2);
	//int m; // address size
	//int C; // cache size(bytes)
	//int B; // block size
	//int E; // set accociativity (blocks per set)
	
	int i = 0;
	int total = 0;
	int status = 0;	
	int numDirty = 0;

	cout << "TEST 1" << endl;
	for(i = 0; i < 16; i++){
		//cout<<"Reading " << i <<endl;
		status = cache.read(i);
		total += status;
	}
	numDirty = cache.countDirty();
	total += numDirty;
	cout << "Total: " << total << endl;
	cout << endl;
	//cache.display();

	Cache cache2(4, 8, 2, 1);
	cout << "TEST 2" << endl;
	status = 0;
	total = 0;
	status = cache2.read(0);
	//cout<<"Reading 0 " << status << endl;	
	total += status;
	status = cache2.read(8);	
	total += status;
	//cout<<"Reading 8 " << status <<endl;
	status = cache2.read(1);
	//cout<<"Reading 1 " << status <<endl;
	total += status;
	status = cache2.read(9);
	total += status;
	//cout<<"Reading 9 " << status <<endl;
	numDirty = cache2.countDirty();
	total += numDirty;
	cout << "Total: " << total << endl;
	cout << endl;
		
	Cache cache3(4, 8, 2, 2);
	cout << "TEST 3" << endl;
	total = 0;
	status = 0;
	for(i = 0; i < 16; i++){
		status = cache3.read(i);
		total += status;
	}
	numDirty = cache3.countDirty();
	total += numDirty;
	cout << "Total: " << total << endl;
	cout << endl;
	//cache3.display();
		
	Cache cache4(4, 8, 2, 2);
	cout << "TEST 4" << endl;
	status = 0;
	total = 0;
	status = cache4.read(0);
	total += status;
	status = cache4.read(8);
	total += status;
	status = cache4.read(1);
	total += status;
	status = cache4.read(9);
	total += status;
	numDirty = cache4.countDirty();
	total += numDirty;
	cout << "Total: " << total << endl;
	cout << endl;

	Cache cache5(4, 8, 2, 2);
	cout << "TEST 5" << endl;
	status = 0;
	total = 0;
	status = cache5.read(0);
	total += status;
	status = cache5.read(4);
	total += status;
	status = cache5.read(8);
	total += status;
	status = cache5.read(0);
	total += status;
	numDirty = cache5.countDirty();
	total += numDirty;
	cout << "Total: " << total << endl;
	cout << endl;
	
	Cache cache6(4, 8, 2, 2);
	cout << "TEST 6" << endl;
		status = 0;
	total = 0;
	status = cache6.read(0);
	total += status;
	status = cache6.write(0);
	total += status;
	status = cache6.read(4);
	total += status;
	status = cache6.read(8);
	total += status;
	numDirty = cache6.countDirty();
	total += numDirty;
	cout << "Total: " << total << endl;
	cout << endl;
			
	Cache cache7(16, 256, 16, 4);
	cout << "TEST 7" << endl;
	total = 0;
	status = 0;
	for(i = 0; i < 8192; i++){
		status = cache7.read(i);
		total += status;
		status = cache7.write(i);
		total += status;
	}
	numDirty = cache7.countDirty();
	total += numDirty;
	cout << "Total: " << total << endl;
	cout << endl;
	
	Cache cache8(16, 256, 16, 4);
	cout << "TEST 8" << endl;
	total = 0;
	status = 0;
	for(i = 0; i < 8192; i+=16){
		status = cache8.read(i);
		total += status;
		status = cache8.write(i);
		total += status;
	}
	numDirty = cache8.countDirty();
	total += numDirty;
	cout << "Total: " << total << endl;
	cout << endl;		
	
}