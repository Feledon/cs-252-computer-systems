#ifndef BLOCK_H
#define BLOCK_H
class Block {
	private:
		int B; // block size(bytes)
		int tag;
		int setIndex;
		int blockOffset;
		
		int valid;
		int dirty;
		int hour;
		int min;
		int sec;
		int usec;
	
	public:
		Block(int blockSize, int setIndex);
		int getValid();
		int getTag();
		int getSetIndex();
		int getBlockOffset();
		int getHour();
		int getMin();
		int getSec();
		int getUsec();
		int getDirty();
		void setValid();
		void setTag(int num);
		void setSetIndex(int num);
		void setBlockOffset(int num);
		void setDirty();
		void clearDirty();
		void updateTime();
};

#endif