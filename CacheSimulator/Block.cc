#include <iostream>
#include "Block.h"
#include <sys/time.h>

using namespace std;

Block::Block(int blockSize, int setIndex) {
	this->B = blockSize;
	this->tag = 0;
	this->valid = 0;
	this->blockOffset = 0;
	this->dirty = 0;
	this->setIndex = setIndex;
	//cout << "Block: blockSize =" << blockSize << " setIndex =" << setIndex << endl;
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	gettimeofday(&tv, &tz);
	struct tm* tm;
	tm = localtime(&tv.tv_sec);
	hour = tm->tm_hour;
	min = tm->tm_min;
	sec = tm->tm_sec;
	usec = tv.tv_usec;
}

int Block::getValid(){
	return valid;
}

int Block::getTag(){
	return tag;
}

int Block::getSetIndex(){
	return setIndex;
}

int Block::getBlockOffset(){
	return blockOffset;
}
int Block::getHour(){
	return hour;
}
int Block::getMin(){
	return min;
}
int Block::getSec(){
	return sec;
}
int Block::getUsec(){
	return usec;
}

int Block::getDirty(){
	return dirty;
}

void Block::setValid(){
	this->valid = 1;
}

void Block::setTag(int num){
	this->tag = num;
}

void Block::setSetIndex(int num){
	this->setIndex = num;
}

void Block::setBlockOffset(int num){
	this->blockOffset = num;
}

void Block::setDirty(){
	this->dirty = 1;
}
void Block::clearDirty(){
	this->dirty = 0;
}

void Block::updateTime(){
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	gettimeofday(&tv, &tz);
	struct tm* tm;
	tm = localtime(&tv.tv_sec);
	hour = tm->tm_hour;
	min = tm->tm_min;
	sec = tm->tm_sec;
	usec = tv.tv_usec;
}
