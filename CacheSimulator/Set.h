#include <vector>
#include "Block.h"
#ifndef SET_H
#define SET_H

class Set {
	private:
		int numBlocks;
		int blockSize;
		int E;
		std::vector <Block> blocks;
	
	public:
		Set(int numBlocks, int blockSize, int setNum);
		std::vector <Block> createSet(int setNum);
		int read(int tag, int setIndex, int blockOffset);
		int write(int tag, int setIndex, int blockOffset); 
		void display();
		int countDirty();
};

#endif