#include <vector>
#include "Set.h"

class Cache {
	private:
		int m; // address size
		int C; // cache size(bytes)
		int B; // block size
		int E; // set accociativity (blocks per set)
		int numSets;
		std::vector <Set> sets;
		int numDirty;
	
	public:
		Cache(int m, int C, int B, int E);
		std::vector <Set> createCache();
		void display();
		int countDirty(); 
		int read(unsigned long int address);
		int write(unsigned long int address);
		int getNumDirty();
		int getNumSets();
		
};