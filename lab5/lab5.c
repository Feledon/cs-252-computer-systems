#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int binToDec(char* bin){
   int dec = 0;
   int q,r = 0;
   int len = strlen(bin);
   int i = len - 1;
   
   for(i=0; i<len; i++){
     if(bin[i] == '1' || dec > 0){  
       q = (bin[i]=='1')?1:0;  
       dec = (dec * 2) + q; 
     }
   }
   return dec;
}//END BINTODEC

char* decToBin(int dec){
   char* bin = (char*)malloc(8);
   int i,d,count = 0;
      
   for(i=7; i>=0; i--) {
      d = dec >> i;
      if(d & 1){
	bin[count] = 1 + '0';
      }else{
	bin[count] = 0 + '0';
      }
      count++;
   }   
   bin[count] = '\0';
   return bin;
}//END DECTOBIN

char* decToBase(int base, int dec){
  char* out = (char*)malloc(sizeof(32));
  char* temp = (char*)malloc(sizeof(32));
  int r = 0;
  int q = 0;
  int i = 0;
  int j = 0;
  q = dec;

  while(q > 0){
    r = q%base;
    q = q/base;
	sprintf(&temp[i], "%d", r);
    i++;
  }
 temp[i] = '\0'; 

 
 for(i;i>0;i--){
 printf("i = %c\n",temp[i]);
  strcat(out,&temp[i]);
  //sprintf(&out[j], "%c", &temp[i]); 
  printf("%c\n",out[i]);
  j=j+1;
  }
  out[j] = '\0'; 
  
  //free(temp);
  return out;
}

void main(){
  int dec = 3;
  /**scanf("%d",&dec);
  char* binResult = decToBin(dec);
  printf("%d -> %s\n", dec, binResult);

  int decRes = binToDec(binResult);
  printf("%s -> %d\n", binResult, decRes);
**/
  dec = 21;
  int base = 2;
  char* generic = decToBase(base, dec);  
  printf("base %d -> %d = %s\n", base, dec, generic);
  base = 8;
  generic = decToBase(base, dec);  
  printf("base %d -> %d = %s\n", base, dec, generic);
  
  base = 16;
  generic = decToBase(base, dec);  
  printf("base %d -> %d = %s\n", base, dec, generic);
  
  free(generic);
}
