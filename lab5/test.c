#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* decToBin(int dec){
   char* bin = (char*) malloc(8 * sizeof(char));
   int i, r, q = 0;
   q = dec;
   
   while(q > 0){
     r = q % 2;
     q = q / 2;
	 sprintf(&bin[i], "%d", r);
	 printf("HERE: %c\n",bin[i]);
     i++;
   }   
   bin[i] = '\0'; 
   return bin;
}//END DECTOBIN

void main(){
  int dec = 11;
  scanf("%d", &dec);
  char* binResult = decToBin(dec);
  printf("%d -> %s\n", dec, binResult);
}
