#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int binToDecGeneric(char* bin){
   int dec = 0;
   int q,n,r = 0;
int num = strtold(bin,NULL);
 int rem,sum=0,power=0;
printf("%d", num);
while(num>0)
 {
 rem = num%10;
 num = num/10;
// sum = sum + rem * pow(2,power);
 power++;
 }



   return sum;
}//END BINTODEC



int binToDec(char* bin){
   int dec = 0;
   int q,r,i = 0;
   int len = strlen(bin);
   for(i=0; i<len; i++){
     if(bin[i] == '1' || dec > 0){  
       q = (bin[i]=='1')?1:0;  
       dec = (dec * 2) + q; 
     }
   }
   return dec;
}//END BINTODEC

char* decToBin(int dec){
   char* bin = (char*)malloc(8);
   int i,d,count = 0;

   for(i=7; i>=0; i--) {
      d = dec >> i;
      if(d & 1){
	bin[count] = 1 + '0';
      }else{
	bin[count] = 0 + '0';
      }
      count++;
   }   
   bin[count] = '\0';
   return bin;
}//END DECTOBIN

void main(){
  int dec = 3;
  scanf("%d",&dec);
  char* binResult = decToBin(dec);

  printf("%d -> %s\n", dec, binResult);
  int decRes = binToDecGeneric(binResult);
  printf("%s -> %d\n", binResult, decRes);
  free(binResult);
}
