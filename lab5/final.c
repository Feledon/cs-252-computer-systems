#include <stdlib.h>
#include <stdio.h>

int arrayLength(char* bin){             
	int i = 0;
	int count = 0;
	while((*(bin + i)) > 0){
		count++;
		i++;
	}
	return count;
}//END ArrayLength

int binToDec(char* bin){		
	int charLength = arrayLength(bin);
	int mult = 1;
	int num = 0;
	int i = charLength - 1;
	for(i; i >= 0; i--){
		if(((*(bin + i)) - 48) == 1){
			num = num + (((*(bin + i)) - 48) * mult);
		}
		mult = mult * 2;
	}
	return num;
}//END BinToDec

char* decToBin(int dec){
	int mult = 1;
	char* temp = (char*)malloc(10*sizeof(char));
	int i = 0;
	int tempMult = 1;
	while(tempMult < dec){
		mult = tempMult;
		tempMult = tempMult * 2;
	}
	while(dec > 0){
		if((dec / mult) >= 1){
			dec = dec - mult;
			temp[i] = '1';
		}else{
			temp[i] = '0';
		}				
		mult = mult / 2;
		i++;		
	}

	return temp;
}//END DecToBin

char* decToBase(int base, int dec){
	int mult = 1;
	char* temp = (char*)malloc(10*sizeof(char));
	int i = 0;
	int tempMult = 1;

	while(tempMult < dec){
		mult = tempMult;
		tempMult = tempMult * base;
	}
	while(dec > 0){
		int answer = 0;
//printf("dec = %d/ mult = %d\n",dec,mult);	
//printf("answer = %d\n",answer);
		if((dec / mult) >= 1){
			answer = (dec / mult);//2
			dec = dec - (mult * answer);//0
			if(answer == 10){
					temp[i] = 'A';
			}else if(answer == 11){
					temp[i] = 'B';
			}else if(answer == 12){
					temp[i] = 'C';
			}else if(answer == 13){
					temp[i] = 'D';
			}else if(answer == 14){
					temp[i] = 'E';
			}else if(answer == 15){
					temp[i] = 'F';
			}else{
					sprintf(&temp[i], "%d", answer);
			}
		}else{
				temp[i] = '0';
		}
		mult = mult / base;
		i++;
	}
	return temp;
}//END DecToBase

int baseToDec(int base, char* bin){		
	int charLength = arrayLength(bin);
	int mult = 1;
	int num = 0;
	int i = charLength - 1;
	for(i; i >= 0; i--){
		if(((*(bin + i)) - 55) >= 10){
			num = num + (((*(bin + i)) - 55) * mult);
			mult = mult * base;
		}else{
			num = num + (((*(bin + i)) - 48) * mult);
			mult = mult * base;
		}
	}
return num;
}//END BaseToDec

void main(){
  int dec = 3;
  char* decBaseEight;
  char* decBaseSixteen;
  char* binResult;
    
/**   int dec1 = baseToDec(2, "11001");
   int dec2 = baseToDec(8, "157");
   int dec3 = baseToDec(16, "f8");
   printf("BASE two -> ten = 11001 -> %d\n", dec1);
   printf("BASE eight -> ten = 157 -> %d\n", dec2);
   printf("BASE two -> sixteen = f8 -> %d\n", dec3);
**/   
  
  scanf("%d",&dec);
	do{  
	  binResult = decToBase(2,dec);
	  printf("BASE ten -> two = %d -> %s\n",dec, binResult);
	  int decRes = binToDec(binResult);
	  printf("BASE two -> ten = %s -> %d\n", binResult, decRes);
	  
	  decBaseEight = decToBase(8,dec);
          printf("BASE ten -> eight = %d -> %s\n", dec, decBaseEight);
	  dec = baseToDec(8, decBaseEight);
	  printf("BASE eight -> ten = %s -> %d\n",decBaseEight, dec);
	  
	  decBaseSixteen = decToBase(16,dec);
          printf("BASE ten -> sixteen = %d -> %s\n", dec, decBaseSixteen);
	  dec = baseToDec(16, decBaseSixteen);
	  printf("BASE sixteen -> ten = %s -> %d\n\n",decBaseSixteen, dec);
	  
	  free(decBaseSixteen);
	  free(decBaseEight);
	  free(binResult); 
	  scanf("%d",&dec);
  } while(dec > 0);

}
