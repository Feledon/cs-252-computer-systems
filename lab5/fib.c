 #include <stdio.h>
 #include <stdlib.h>
 #include <math.h>

        int fib(int n){

                int answer = 0;
                int n1 = 0;
                int n2 = 1;

                int i = 0;
                for(i; i < n; i++){
                        n1 = n2;
                        n2 = answer;
                        answer = (n1 + n2);
                }

                return answer;

        }


        int maxInt(){

                int answer = 0;
                int n1 = 0;
                int n2 = 1;
                double n3 = 0;

                while(n3 >= 0){
                        n1 = n2;
                        n2 = n3;
                        answer = n3;
                        n3 = (n1 + n2);
                }

                return answer;

        }

        long maxLong(){

                long answer = 0;
                long n1 = 0;
                long n2 = 1;
                double n3 = 0;

                while(n3 >= 0){
                        n1 = n2;
                        n2 = n3;
                        answer = n3;
                        n3 = (n1 + n2);
                }


                return answer;

        }

        void finRep(){
                //Changed double to long for less precision
                long num0 = 0.2;
                long num1 = 0.2;
                long num2 = 0.2;
                long num3 = 0.6;
                long num5;
                num5 = (num0 + num1 + num2 - num3);

                printf("num5: %d\n", num5);
                if(num5 == 0){
                printf("%f is 0!\n", num5);
                }else{
                printf("%f is not 0!\n", num5);
                }

                if(num5 == 0){
                printf("%.20f is 0!\n", num5);
                }else{
                printf("%.20f is not 0!\n", num5);
                }

        }

        int arrayLength(char* bin){             
                int i = 0;
                int count = 0;
                
                while((*(bin + i)) > 0){
                        count++;
                        i++;
                }
                
                return count;
        }

        int binToDec(int base, char* bin){
                
                int charLength = arrayLength(bin);
                int mult = 1;
                int num = 0;
                int i = charLength - 1;

                for(i; i >= 0; i--){
                        if(((*(bin + i)) - 48) == 1){
                                num = num + (((*(bin + i)) - 48) * mult);
                        }
                        mult = mult * base;
                }


                return num;
        }

        char* decToBin(int dec){
                
                int mult = 1;

                char* temp = (char*)malloc(10*sizeof(char));
                int i = 0;
                int tempMult = 1;

                while(tempMult < dec){
                        mult = tempMult;
                        tempMult = tempMult * 2;
                }
                        
                while(dec > 0){
                        if((dec / mult) >= 1){
                                dec = dec - mult;
                                temp[i] = '1';
                        }else{
                                temp[i] = '0';
                        }
                mult = mult / 2;
                i++;
                }
                return temp;
        }


        char* decToBase(int base, int dec){
                int mult = 1;

                char* temp = (char*)malloc(10*sizeof(char));
                int i = 0;
                int tempMult = 1;
                                
                        while(tempMult < dec){
                                mult = tempMult;
                                tempMult = tempMult * base;
                        }
                                
                        while(dec > 0){
                                int answer = 0;
                                if((dec / mult) >= 1){
                                        answer = (dec / mult);
                                        dec = dec - (mult * answer);
                                        if(answer == 10){
                                                temp[i] = 'A';
                                        }else if(answer == 11){
                                                temp[i] = 'B';
                                        }else if(answer == 12){
                                                temp[i] = 'C';
                                        }else if(answer == 13){
                                                temp[i] = 'D';
                                        }else if(answer == 14){
                                                temp[i] = 'E';
                                        }else if(answer == 15){
                                                temp[i] = 'F';
                                        }else{
                                                sprintf(&temp[i], "%d", answer);
                                        }
                                }else{
                                        temp[i] = '0';
                                }
                        mult = mult / base;
                        i++;
                        }

                return temp;
        }

        int baseToDec(int base, char* bin){
                
                int charLength = arrayLength(bin);
                int mult = 1;
                int num = 0;
                int i = charLength - 1;

                for(i; i >= 0; i--){
                        if(((*(bin + i)) - 55) >= 10){
                                num = num + (((*(bin + i)) - 55) * mult);
                                mult = mult * base;
                        }else{
                                num = num + (((*(bin + i)) - 48) * mult);
                                mult = mult * base;
                        }
                }


                return num;
        }