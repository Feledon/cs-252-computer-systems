import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream.GetField;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class Master {
	static Integer beg = new Integer(1000);
	static Integer end = new Integer(10000);
	static Integer numNodes = 3;//MAX AT 5
	static Socket[] s = new Socket[numNodes];
	static ArrayList<Integer> list = new ArrayList<Integer>();
	static String[] nodeNames = {"localhost", "localhost", "localhost", "localhost", "localhost"};
	static int[] nodePorts = {1000, 2000, 3000, 4000, 5000};
	private static int[] x;
	static int j = 0;
	static Node node = null;
	private static int total;

	private static void startNodes(final int thisNode){
		Thread temp = new Thread(new Runnable() {
			@Override
			public void run() {
				//				System.out.println(nodePorts[i]);
				node = new Node(nodePorts[thisNode]);
				node.start();//Fires up the node for the passed port and prepares it to do our work
			}
		});
		temp.start();
	}

	public static void main(String[] args) {		
		try {
			ObjectOutputStream oos;
			final ObjectInputStream[] ois = new ObjectInputStream[numNodes];
			Thread[] threads = new Thread[numNodes];
			x = new int[numNodes];

			for(j = 0; j < numNodes; j++){				
				list = new ArrayList<Integer>();
				list.add(beg);
				list.add(end);
				list.add(numNodes);
				list.add(j);//nodeNum
				//System.out.println("Master about to send node #" + i);

				//Make a bunch of nodes for our requests and fire em up
				startNodes(j);

				s[j] = new Socket(nodeNames[j], nodePorts[j]);				
				oos = new ObjectOutputStream(s[j].getOutputStream());				
				oos.writeObject(list);
				ois[j] = new ObjectInputStream(s[j].getInputStream());
				threads[j] = new Thread(new Runnable() {					
					@Override
					public void run() {
						try {
//							System.out.println(j);
							x[j - 1] = (int) ois[j - 1].readObject();//TODO:WTF is this 1 higher than expected?
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				threads[j].start();				
			}

			for(int i = 0; i < numNodes; i++){
				try {
					threads[i].join();
					total += x[i];
					System.out.println("Joined #" + i + " for " + x[i]);	
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				//				
				s[i].close();
			}

			System.out.println("Grand Total: " + total);

			//s.close();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
