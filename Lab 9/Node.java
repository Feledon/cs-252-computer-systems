import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


public class Node {
	static ServerSocket ss;
	Node(int socket){
		try {
			this.ss = new ServerSocket(socket);
		} catch (IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return;	
	}

	public void start(){
		this.main(null);
	}

	public static void main(String[] args) {

		try {
			//Need to restart for each damn node we are looking for
			Socket s = ss.accept(); // blocks until I get a call

			ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
			ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());

			//Retrieve values
			ArrayList<Integer> list = (ArrayList<Integer>) ois.readObject() ;
			int beg = list.get(0);
			int end = list.get(1);
			Integer numNodes = list.get(2);
			Integer nodeNum = list.get(3);
			
			Integer totalPrimes = findPrimeInRangeStriping(beg, end, nodeNum, numNodes);
//			Integer totalPrimes = findPrimeInRange(beg, end, nodeNum, numNodes);
			
			oos.writeObject(totalPrimes);			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static int findPrimeInRangeStriping(int beg, int end, int nodeNum, int numNodes){
		int localPrimeCount = 0;
		while(beg < end){		
			if(isPrime(beg + nodeNum)){
				localPrimeCount++;
			}
			beg += numNodes;
		}	
		
		System.out.println("Thread #" + nodeNum + " multiples of " + beg + " + " + nodeNum + " found " + localPrimeCount);
		
		return localPrimeCount;
	}
		
	public static int findPrimeInRange(int beg, int end, int nodeNum, int numNodes){
		int localPrimeCount = 0;
		//Blocking code for the ranges
		int start = beg;
		if(nodeNum != 0){
			start = (int)(((double)end / numNodes) * nodeNum);
		}
		int stop = (int)(((double)end / numNodes) * (nodeNum + 1));
		
		while(start < stop){		
			if(isPrime(start)){
				localPrimeCount++;
			}
			start++;
		}		
		System.out.println("Thread #" + nodeNum + " found " + localPrimeCount + " in the range " + start + " to " + stop);
		return localPrimeCount;
	}//END findPrimeInRange


	private static boolean isPrime(int n){//check if n is prime and return T/F	
		int d = 2;	
		boolean prime = true;

		while(d < n && prime){		
			if((n % d) == 0){
				prime = false;
			}else{
				d++;
			}		
		}

		return prime;	
	}//END findPrime	
}
