#include <stdio.h>
#include <stdbool.h>

typedef struct freq {
   int num;
   int count;
} freq;

void readScores(int* ar, int* size);
void displayScores(int* ar, int* size);
void calcHistogram(freq* structAr, int* structSize, int* ar, int* size);
void displayHistograms(freq* ar, int* size);
void sortHistograms(freq* ar, int* size);

int main(){

   int ar[100];
   int size = 0;

   readScores(ar, &size);
   displayScores(ar, &size);
  
   freq hist[size];
   int histSize = 0;
   
printf("\n");
   calcHistogram(hist, &histSize, ar, &size);
   displayHistograms(hist, &histSize);
  
   sortHistograms(hist, &histSize);
printf("\n");
   displayHistograms(hist, &histSize);
}//END MAIN

void displayHistograms(freq* ar, int* size){
   int i = 0;
   for(i;i<*size;i++){
      freq curr = (*(ar + i));    
      printf("value %d freq %d\n", curr.num, curr.count);
   }
}
//END displayHistograms

void sortHistograms(freq* ar, int* size){
int i = 0;
int j = 0;
int minIndex = 0;
for(i; i< *size; i++){
   minIndex = i;
   for(j = i; j < *size; j++){
      if((*(ar + minIndex)).count < (*(ar + j)).count){
         minIndex = j;
      }
   }

   freq temp = (*(ar + i));

//      printf("value %d freq %d\n", temp.num, temp.count);
   (*(ar + i)) = (*(ar + minIndex));
   (*(ar + minIndex)) = temp;
}
}//END sortHistograms

void calcHistogram(freq* structAr, int* structSize, int* ar, int* size){
int i = 0;
int j = 0;

for(i; i <= *size; i++){
   int num = *(ar + i);
   bool found = false;

   for(j = 0;j <= *structSize; j++){
     int thisNum = (*(structAr + j)).num;
     if(num == thisNum){
        (*(structAr + j)).count += 1;
        found = true;   
     }
   }
   if(!found){
      *structSize += 1;
      (*(structAr + *structSize)).num = num;
      (*(structAr + *structSize)).count = 1;
   }

}
}//END calcHistogram

void displayScores(int* ar, int* size){
   int i = 0;
   for(i;i<*size;i++){
      printf("%d\n", *(ar + i));
   }
}//END displayScores

void readScores(int* ar, int* size){
   int test;
   int i = 0;

   for(i;i<100;i++){
      if(scanf("%d", test) != 1){      
         break;
      }
     *(ar + i) = test;
   }
  *size = i;
}//END readScores
