#include "linkedList.h"

int llRemoveAtIndex(LinkedList** LL, int index){
  LinkedList* temp;
  LinkedList* nn;
  int i = 0;
	if(*LL == NULL){
		return 0;
	}
	if(index == 0){
		nn = *LL;
		*LL = (*LL)->next;		
	}else{
		nn = *LL;
		for(i = 0; i < index - 1; i++){
			nn = nn->next;
		}
	temp = nn->next;
	nn->next = nn->next->next;	
  }
  return 1;
}//END llRemoveAtIndex

void llAddAtIndex(LinkedList** LL, tnode* value, int index) {	
  LinkedList* nn;
  nn  = (LinkedList*)malloc(sizeof(LinkedList));
  LinkedList* p = *LL;
  nn->value = value;
  if (*LL == NULL) {
    *LL = nn;  
  }else{
	int i = 0;
	while ( i<index-1 && p->next != NULL) {
		p = p->next;
		i++;
	}
	nn->next = p->next;
	p->next = nn;
	}
}//END llAddAtIndex

void list_add_in_order(LinkedList** LL, tnode* passedNode) {
	tnode** ptrNode = (tnode**)malloc(sizeof(tnode));
	*ptrNode = passedNode;
	int index = 0;	
 //printf("here %c\n", ((passedNode))->c);	
	LinkedList* p = *LL;	 
	//printf("here\n");
    if (p == NULL) {
		llAddAtIndex(LL, *ptrNode, 0);
	} else {
		if ( p->next == NULL) {
			llAddAtIndex(LL, *ptrNode, 0);
		} else {
			tnode* tmpNextNode = p->next->value;
			int currentWeight = passedNode->weight;
			int nextWeight = tmpNextNode->weight;
			while(currentWeight >= nextWeight && (p->next != NULL)) {
				p = p->next;
				nextWeight = (p)->value->weight;
				index++;
			}
			llAddAtIndex(LL, *ptrNode , index);
		}
	}
}//END list_add_in_order

void llDisplay(LinkedList** l){
  LinkedList* p = *l;
  int ct = 0;
  printf("[");
  while(p != NULL){
    if(ct==0){
       printf("%d,", p->value->c);
    }else if(p->next != NULL){
       printf(" %d,", p->value->c);
    }else{
       printf(" %d", p->value->c);
    }
    ct++;
    p = p->next;
  }
  printf("]\n");
}//END llDisplay

int llSize(LinkedList** l){
  int count = 0;
  LinkedList* p = *l;
  
  while(p != NULL) {
    p = p->next;
    count ++;
  }
  return count;
}//END llSize

LinkedList* llCreate(){
  return NULL;
}//END llCreate