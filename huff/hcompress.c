#include "linkedList.h"
#include "hcompress.h"

int main(int argc, char *argv[]) { 
	// Check the make sure the input parameters are correct 
	if (argc != 3) { 
	 printf("Error: The correct format is \"hcompress -e filename\" or \"hcompress -d filename.huf\"\n"); 
	 fflush(stdout); 
	 exit(1); 
	} 

	// Create the frequency table by reading the generic file 
	tnode* leafNodes = createFreqTable("decind.txt"); 
	//displayNodes(leafNodes);
	// Create the huffman tree from the frequency table 
	tnode* treeRoot = createHuffmanTree(leafNodes); 
	//displayNodes(treeRoot);

	// encode 
	if (strcmp(argv[1], "-e") == 0) { 
		// Pass the leafNodes since it will process bottom up 
		encodeFile(argv[2], leafNodes);  
	} else { // decode 
		// Pass the tree root since it will process top down 
		decodeFile(argv[2], treeRoot); 
	} 

	return 0; 
} //END MAIN

tnode* createFreqTable(char* inFile){	
	tnode* out = (tnode*)malloc(128 * sizeof(tnode));	
	FILE *ifp;
	char* mode = "r";
	int i = 0;
	ifp = fopen(inFile, mode);	
	int test = 0;
		
	for(i = 0; i < 128; i++){
     	tnode temp;
		temp.weight = 0;
		temp.c = i;
		temp.left = NULL;
		temp.right = NULL;
		temp.parent = NULL;
	    out[i] = temp;
	}	
			   
	test  = fgetc(ifp);
	while (test != EOF) {				   	   
	   for(i = 0; i < 128; i++){
		 if(out[i].c == test){		
			out[i].weight += 1;	
		 }
	   }   	   
	   test  = fgetc(ifp);
	}
	
	fclose(ifp);	
	return out;
}//END createFreqTable

/**
CreateHuffmanTree should take in this array of leaf nodes and form the Huffman Tree. 
To do so, you will need to use your LinkedList. 
You should place all your nodes in the list in order. 
Then simply repeatedly combine the lowest two nodes, forming a new node,and add that new node back into the list.
When you are done, you should have the root node that you need to return. 
**/
tnode* createHuffmanTree(tnode* leaf) {
	LinkedList* LL = llCreate();
	int v = 0;

	for(v = 0; v < 128; v++) {	
		list_add_in_order(&LL, &leaf[v]);		
	}

	//llDisplay(&LL);
	int i = 0;	
	//LL is ordered from least to most
	while(llSize(&LL) > 1) {	
		LinkedList* p = LL;
		tnode* tmp = (tnode*)malloc(sizeof(tnode));//new node to start from	
		p->value->parent = tmp;
		tmp->weight = p->value->weight + p->next->value->weight;
		tmp->right = p->value;
		tmp->left =  p->next->value;
//printf("loops %d Weight %d Value %d \n",i,p->value->weight,  p->value->c);
		p->next->value->parent = tmp;
		if((LL)->next != NULL ) {
			llRemoveAtIndex(&LL, 0);			
		}
			list_add_in_order(&LL, tmp);
		if( (LL)->next != NULL ) {
			llRemoveAtIndex(&LL, 0);						
		}
		i++;
	}	
//Verify that return is correct	
//printf("Node %d\n",LL->value->right->right->left->c);	
//displayNodes((LL)->value);     
	return LL->value;
}//END CREATEHUFFMANTREE

/**
EncodeFile takes in the name of the file you would like to encode. This could be decind.txt or it could be some other file that has a similar letter distribution as decind.txt. 
As you read in each character from the file you should find that character in the leafNodes array. 
Then you should use the tree that is attached to that leaf node to walk up to the root node and discover the Huffman code that should be used to encode that character. 
Once the number of bits you need to write out goes over 8, you should write out that byte to the .huf file. 
That is, you should not read in the entire file and turn it into a large string of 0s and 1s in memory before writing out. 
You should write it out as you have enough to write out. 
**/
void encodeFile(char argv[] , tnode* leafNodes){
//displayNodes(leafNodes);
    char* outFileName = getFileName(argv, "huf");	
	int c;
	FILE *file;
	FILE * destination;
	file = fopen(argv, "r");
	destination = fopen(outFileName,"wb");
	if (file) {
		while((c = getc(file)) != EOF) {
			int bitLimit = 0;//Counter for the current size			
			unsigned int character = c; //Holder for the current read in character
			unsigned char bits = 0; //outputted huffman
			tnode* tmpChNode = (leafNodes + character);		
			tnode* ParentNode = (leafNodes + character)->parent; 		
			while ((ParentNode->parent != NULL) &&(bitLimit < 8)){		
				if (ParentNode->left == tmpChNode) {
//printf("%c = %c\n",(*tmpChNode).c, ParentNode->left->c);	
					bits = bits<< 1;
					bits += 0;
					bitLimit++;
				}else{
//printf("%d\n", bits);	
					bits = bits<< 1;
					bits += 1;
					bitLimit++;
				}
				if ( ParentNode->parent->left == ParentNode) {
					bits = bits << 1;
					bits += 0;
					bitLimit++;
				} else {
					bits = bits<< 1;
					bits += 1;
					bitLimit++;
					}
				ParentNode = ParentNode->parent;
			}
		//unsigned char ch = bits;
		//printf("%c   %d    \n", bits, bits);
		//printf("CH %c   %d    \n", ch, ch);
			fprintf(destination, "%d", bits);
		}
	}
	fclose(destination);
    fclose(file);
}//END ENCODEFILE

/**
DecodeFile is similar to encode except that it will work top-down from the root of the 
Huffman tree to the leaf nodes. 
**/
void decodeFile(char argv[], tnode* treeRoot){
	char* outFileName = getFileName(argv, "huf.dec");
	int c;
	FILE *file;
	FILE *fp2;
    fp2 = fopen(outFileName,"wb");
	file = fopen(argv, "r");	
	if (file) {
		while ((c = getc(file)) != EOF) {			
			tnode* tmpChNode = treeRoot;
			unsigned int bits = c;			
			int bitCap = 7;
			char decodedCharVal;
			
			while( tmpChNode->right != NULL && tmpChNode->left != NULL ) { 
				unsigned int tmpBits = bits>>bitCap;
				tmpBits = tmpBits & 1;
				
				if( tmpBits == 1 ){
					tmpChNode= tmpChNode->right;
					bitCap--;
				} else { 
					tmpChNode = tmpChNode->left;
					bitCap--;
				} 
				decodedCharVal = tmpChNode->c;
			}
		fprintf(fp2,"%c", decodedCharVal);
//printf("%c  ", decodedCharVal);
		} 
	}	
	fclose(fp2);
	fclose(file);
}

void displayNodes(tnode* nodes){
	int i = 0;
	for(i = 0; i < 128; i++){
	   printf("@%d %c = %d\n",i, nodes[i].c, nodes[i].weight);
	}
}//END DISPLAYNODES

/**
*Returns a new file name, looping through the first string until end or "."
* then appends the passed ext.
**/
char* getFileName(char* fileName, char* newExt){
	char* out = (char*)malloc(sizeof(char));
	bool found = false;
	int i = 0;
	int j = 0;
	
	for(i = 0;!found && i < strlen(fileName);i++){	
	   out[i] = fileName[i];
	   if(fileName[i] == '.'){
	      found = true;
	   }
	}
	
	for(i = i; j < strlen(newExt); i++){
	   out[i] = newExt[j];
	   j++;
	}
	
	return out;
}//END getFileName
