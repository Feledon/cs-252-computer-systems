#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]);	
void displayNodes(tnode* nodes);
tnode* createFreqTable(char* inFile);
tnode* createHuffmanTree(tnode* leaf);
void encodeFile(char argv[] , tnode* leafNodes);
void decodeFile(char argv[], tnode* treeRoot);
char* getFileName(char* fileName, char* newExt);