#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 

typedef struct tnode {
	int weight;
	unsigned int c;
	struct tnode* left;
	struct tnode* right;
	struct tnode* parent;
}tnode;

typedef struct node {
  tnode* value;
  struct node* next;
} LinkedList;

LinkedList* llCreate();
int llSize(LinkedList** l);
void llDisplay(LinkedList** l);
void llAddAtIndex(LinkedList** LL, tnode* value, int index);
int llRemoveAtIndex(LinkedList** LL, int index);
void list_add_in_order(LinkedList** LL, tnode* passedNode);