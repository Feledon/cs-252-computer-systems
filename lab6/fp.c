#include <stdlib.h>
#include <stdio.h>
#include <string.h> 

typedef union fi{
  unsigned int i;
  float f;
}fi;
 
void displayFloat(float f);
float makeFloat(char* f);

char* decToBin(int dec){
	int mult = 1;
	char* temp = (char*)malloc(10*sizeof(char));
	int i = 0;
	int tempMult = 1;
	while(tempMult < dec){
		mult = tempMult;
		tempMult = tempMult * 2;
	}
	while(dec > 0){
		if((dec / mult) >= 1){
			dec = dec - mult;
			temp[i] = '1';
		}else{
			temp[i] = '0';
		}				
		mult = mult / 2;
		i++;		
	}	
	return temp;
}//END DecTo

/** 
Create a main that tests your code by making a call to makeFloat with "-101.1101". 
Print that result as a float. 
Then use that float in the displayFloat to get the printout shown above.
**/
int main(){
	fi huh;	
	huh.i = 0;
	char* flt = "-101.1101";
	huh.f = makeFloat(flt);	
	//huh.f = -5.8125;	
	
	printf("%f\n", huh.f);
	displayFloat(huh.f);
	
	return 0;
}

/**
MakeFloat should take in a string representation of a binary number and create a C float from it. For example, the call:
	float result = makeFloat("-101.1101"); 
	or makeFloat("+101.1101") 
	-- the +/- should always be included should put a value of -5.8125 into result.
**/
float makeFloat(char* f){
	fi floatNum;	
	floatNum.i = 0;
	int i;
printf("Converting: %s\n", f);	
	
	
	/**
	Get the leading character to find the sign
	**/
	if (f[0] == '-'){
		floatNum.i = (1<<31);
	} else if (f[0] == '+'){
		floatNum.i = floatNum.i & ~(1<<31);
	}
	
	/**
	Find the exponent
	**/
	int count = 0;	
	while(f[count] != '.'){//count the characters up to the '.'
		count++;
	}		
	int ex = count + 127 - 2;//This is our bias + exp
	char* exponent = decToBin(ex);//Convert the exponent to binary
printf("Exponent: %s\n", exponent);
	
	/**
	Find the mantissa
	**/
	for(i = 0; i < strlen(exponent); i++){
		if(exponent[i] == '1'){
			floatNum.i = floatNum.i | 1<<(30 - i);
		}
	}
printf("%d\n", count);	
	int counter = 0;
	for(i = 2; i < strlen(f); i++){
		if(f[i] != '.'){
			printf("f[%d]: %c\n", i, f[i]);
			if(f[i] == '1'){
				floatNum.i = floatNum.i | 1<<(22 - counter);
			}
			counter++;		
		}
		
	}
	
	return floatNum.f;
}//END makeFloat

/**
DisplayFloat should take in a C float and display the 32 bits that represent that float to the screen. This is the sign bit, the exponent bits, and the mantissa bits. 
For example, the call:
	displayFloat(-5.8125);
	should print out:
	1 10000001 01110100000000000000000
 **/
void displayFloat(float f){
	fi bit;
	bit.f = f;	
	int i;//32 bits represent the float
	
	for(i = 31; i >= 0; i--){ //Loop through for each bit
		printf("%d", (bit.i >> i) & 1);//Print the bit
		if(i == 31 || i == 23){//Space it out for the sign, exp, and mantissa.
			printf(" ");
		}
	}
	printf("\n");
}//END displayFloat