#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


typedef union fi {
	unsigned int i;
	float f;
}fi;

char* decToBin(int dec){
	int mult = 1;
	char* temp = (char*)malloc(10*sizeof(char));
	int i = 0;
	int tempMult = 1;
	while(tempMult < dec){
		mult = tempMult;
		tempMult = tempMult * 2;
	}
	while(dec > 0){
		if((dec / mult) >= 1){
			dec = dec - mult;
			temp[i] = '1';
		}else{
			temp[i] = '0';
		}				
		mult = mult / 2;
		i++;		
	}	
	return temp;
}//END DecTo

float makeFloat(char* f){
	fi floatNum;
	floatNum.i = 0;

	if (f[0] == '-'){
		floatNum.i = (1<<31);
	} else if (f[0] == '+'){
		floatNum.i = floatNum.i & ~(1<<31);
	}
	
	int count = 0;
	
	while(f[count] != '.'){
		count++;
	}
	
	int exp = count + 127 - 2;
	
	char* exponent = decToBin(exp);
	int i;
	for(i = 0; i < strlen(exponent); i++){
		if(exponent[i] == '1'){
			floatNum.i = floatNum.i | 1<<(30 - i);
		}
	}
	
	int counter = 0;
	for(i = 2; i < strlen(f); i++){
		if(f[i] != '.'){
			printf("f[%d]: %c\n", i, f[i]);
			if(f[i] == '1'){
				floatNum.i = floatNum.i | 1<<(22 - counter);
			}
			counter++;		
		}
		
	}
	
	return floatNum.f;
}

void displayFloat(float f){
	fi bit;
	bit.f = f;
	
	int i = 31;
	for(i; i >= 0; i--){
		printf("%d", (bit.i >> i) & 1);
		if(i == 31 || i == 23){
			printf(" ");
		}
	}
	printf("\n");
}

void main(){
/**
	float answer = makefloat("-101.1101");
	printf("%f\n", answer);
	displayFloat(answer);
**/	
	fi huh;	
	huh.i = 0;
	char* flt = "-101.1101";
	huh.f = makeFloat(flt);	
	//huh.f = -5.8125;	
	
	printf("%f\n", huh.f);
	displayFloat(huh.f);

}