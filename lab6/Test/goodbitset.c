#include <stdio.h>
#include <stdlib.h>

typedef unsigned short bitSet;

bitSet makeBitSet(){
	return 1;
}

void setBit(bitSet* bs, int index){
	*bs = *bs | (1 << index); 
}

void clearBit(bitSet* bs, int index){
	*bs &= ~(1 << index);
}

int bitValue(bitSet bs, int index){
	return(bs >> index & 1);
}

void displayBitSet(bitSet bs){
	int i;
	for(i = 16; i >= 0; i--){
		printf("%hd", bitValue(bs,i));
	}
	printf("\n");
}

void main(){
	int n;

	bitSet bit = makeBitSet();
	bit.i = 0;
	setBit(&bit, 2); 
	displayBitSet(bit);
	clearBit(&bit, 2); 
	displayBitSet(bit);
	int test  = bitValue(bit, 2);
}