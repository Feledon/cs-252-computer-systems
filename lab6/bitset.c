#include <stdlib.h>
#include <stdio.h>

typedef unsigned short bitSet;

bitSet makeBitSet(); // Create a new bitset
void setBit(bitSet* bs, int index); // Sets bit 'index' of the bitset to 1
void displayBitSet(bitSet bs); // Displays the 16 bits of the bitset to the screen
void clearBit(bitSet* bs, int index); // Sets bit 'index' of the bitset to 0
int bitValue(bitSet bs, int index); // Returns the value of the bit at 'index'

void main(){
  bitSet bit = makeBitSet();
    
  setBit(&bit, 3);
  setBit(&bit, 0);
  displayBitSet(bit);
    
  printf("set @ 3 and 0  = %d\n\n", bit);
  
  clearBit(&bit, 3);
  displayBitSet(bit);    
  printf("Switched back @ 3 = %d\n\n", bit);
  
  clearBit(&bit, 0);  
  displayBitSet(bit);   
  printf("switched back @ 0 = %d\n\n", bit);
  
}

// Returns the value of the bit at 'index'
int bitValue(bitSet bs, int index){
    return(bs >> index & 1);
}

// Sets bit 'index' of the bitset to 0
void clearBit(bitSet* bs, int index){
   bitSet mask = (1 << index);
   //for 2 the mask = 0000 0100
   //not(~) the mask(1111 1011) , then run an and(&) to set to '0'.
   //1111 1011 not of index 2(sets 4 = 0)
   //0000 1100 test 12
   //0000 1000 output 8 from '&' operator
  *bs = *bs & ~mask;
} 

// Sets bit 'index' of the bitset to 1
void setBit(bitSet* bs, int index){  
  bitSet bit = (1 << index);//for 2 = 0000 0100
  // | takes new set, and compares old set to get a new value
  *bs = *bs | bit; 
}

// Displays the 16 bits of the bitset to the screen
void displayBitSet(bitSet bs){
	int i;
	for(i = 16; i >= 0; i--){
		printf("%hd", bitValue(bs,i));
	}
	printf("\n");
}

// Create a new bitset
bitSet makeBitSet(){
  return 0;
}